import NavbarPage from '../pom/NavbarPage'
import RegistrationPage from '../pom/RegistrationPage'
import {Browser, test as baseTest} from '@playwright/test'
import LoginPage from '../pom/LoginPage';
import AccountPage from '../pom/AccountPage';
import EditYourAccountInformationPage from '../pom/EditYourAccountInformationPage';

const test = baseTest.extend<{
    navbarPage: NavbarPage;
    registrationPage: RegistrationPage;
    loginPage: LoginPage;
    accountPage: AccountPage;
    editYourAccountInformationPage: EditYourAccountInformationPage;
    browser: Browser;
}>({
    navbarPage: async({page}, use) => {
        await use(new NavbarPage(page));
    },
    registrationPage: async({page}, use) => {
        await use(new RegistrationPage(page));
    },
    loginPage: async({page}, use) =>{
        await use(new LoginPage(page));
    }, 
    accountPage: async({page}, use) => {
        await use(new AccountPage(page))
    },
    editYourAccountInformationPage: async({page}, use) => {
        await use(new EditYourAccountInformationPage(page))
    },
})
export default test;
export const expect = test.expect;