import test, { expect } from "../fixtures/basePages";
const loginData = JSON.parse(JSON.stringify(require('../data/editData.json')));
const regData = JSON.parse(JSON.stringify(require('../data/registrationData.json')));

test.describe.serial('Edit your account information suite', () => {
    test.beforeEach(async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.goToEditYourAccountInformationPage();
    });
    test.afterEach(async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.close();
    });
    
    test('Verify that First name field is visible EI - 001', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.firstNameIsVisible();
    });
    test('Verify that Last name field is visible EI - 002', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.lastNameIsVisible();
    });
    test('Verify that Email field is visible EI - 003', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.emailIsVisible();
    });
    test('Verify that Telephone field is visible EI - 004', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.telephoneIsVisible();
    });
    test('Verify that First name field has value EI - 005', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.firstNameHasValue();
    });
    test('Verify that Last name field has value EI - 006', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.lastNameHasValue();
    });
    test('Verify that Email field has value EI - 007', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.emailHasValue();
    });
    test('Verify that Telephone field has value EI - 008', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.telephoneHasValue();
    });
    test('Verify that First name field is editable EI - 009', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.firstNameIsEditable();
    });
    test('Verify that Last name field is editable EI - 010', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.lastNameIsEditable();
    });
    test('Verify that email field is editable EI - 011', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.emailIsEditable();
    });
    test('Verify that telephone field is editable EI - 012', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.telephoneIsEditable();
    });
    test('Verify email field format EI - 013', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.emailFieldFormat();
    });
    test('Verify telephone field format EI - 014', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.telephoneFieldFormat();
    });
    test('Verify that value from the json is equal to the value in the field First name EI - 015', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.firstNameEqual(loginData[0].firstName);
    });
    test('Verify that value from the json is equal to the value in the field Last name EI - 016', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.lastNameEqual(loginData[0].lastName);
    });
    test('Verify that value from the json is equal to the value in the field Email EI - 017', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.emailEqual(regData[0].email);
    });
    test('Verify that value from the json is equal to the value in the field Telephone EI - 018', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.telephoneEqual(loginData[0].telephone);
    });
    test('Edit the empty value of the First name field EI - 023', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.editFirstNameEmptyValue();
    });
    test('Edit the empty value of the Last name field EI - 024', async ({ editYourAccountInformationPage }) => {
        await editYourAccountInformationPage.editLastNameEmptyValue();
    });
});
