import test, { expect } from "../fixtures/basePages";
import ENV from '../utils/env';

const registerData = JSON.parse(JSON.stringify(require('../data/registrationData.json')));


test.describe('Registration test suite', () => {
    test.use({storageState: 'NoAuth.json'})
    test.beforeEach(async ({ page, navbarPage }) => {
        await page.goto(ENV.BASE_URL);
        await navbarPage.clickOnRegister();
    })
    test.afterEach(async ({ page }) => {
        await page.close();
    })
        test.skip('Verify that email field is correct format R-T001', async ({ registrationPage }) => {
            await registrationPage.emailFieldFormat();
        });
        test.skip('Verify that telephone field is correct format R-T002', async ({ registrationPage }) => {
            await registrationPage.telephoneFieldFormat();
        });
        test.skip('Verify that email address is in correct format R-T003', async ({ registrationPage }) => {
            await registrationPage.emailAddressCorrectFormat(registerData[0].email);
        });
        test.skip('Verify that telephone is in correct format R-T004', async ({ registrationPage }) => {
            await registrationPage.phoneAddressCorrectFormat(registerData[0].telephone);
        });
        test.skip('Verify that email address is not in correct format R-T005', async ({ registrationPage }) => {
            await registrationPage.emailAddressNotCorrectFormat(registerData[2].email);
        });
        test.skip('Verify that first name field is visible and editable R-T006', async ({ registrationPage }) => {
            await registrationPage.firstNameIsVisible();
            await registrationPage.firstNameIsEditable();
        });
        test.skip('Verify that last name field is visible and editable R-T007', async ({ registrationPage }) => {
            await registrationPage.lastNameIsVisible();
            await registrationPage.lastNameIsEditable();
        });
        test.skip('Verify that email field is visible and editable R-T008', async ({ registrationPage }) => {
            await registrationPage.emailFiledIsVisible();
            await registrationPage.emailFieldEditable();
        });
        test.skip('Enter an email with white spaces R-T009', async ({ registrationPage }) => {
            await registrationPage.enterRegistrationEmail(registerData[3].email);
        });
        test.skip('Verify that telephone field is visible and editable R-T010', async ({ registrationPage }) => {
            await registrationPage.phoneIsVisible();
            await registrationPage.phoneIsEditable();
        });
        test.skip('Enter empty telephone field R-T011 ', async ({ registrationPage }) => {
            await registrationPage.enterTelephoneField(registerData[1].telephone);
        });
        test.skip('Enter a password with white spaces R-T012', async ({ registrationPage }) => {
            await registrationPage.enterTelephoneFieldWhiteSpaces(registerData[3].telephone);
        });
        test.skip('Verify that password field is visible and editable R-T013', async ({ registrationPage }) => {
            await registrationPage.passwordFieldIsVisible();
            await registrationPage.passwordFieldIsEditable();
        });
        test.skip('Vefiry that password filed hides the characters R-T014', async ({ registrationPage }) => {
            await registrationPage.registrationPasswordFieldCharsHidden();
        });
        test.skip('Enter an empty password field R-T015', async ({ registrationPage }) => {
            await registrationPage.enterPasswordField(registerData[1].password);
        });
        test.skip('Verify that confirm password field is visible and editable R-T016', async ({ registrationPage }) => {
            await registrationPage.confirmPasswordIsVisible();
            await registrationPage.confirmPasswordIsEditable();
        });
        test.skip('Vefiry that confirm password filed hides the characters R-T017', async ({ registrationPage }) => {
            await registrationPage.confirmPasswordFieldCharsHidden();
        });
        test.skip('Enter an empty confirm password field R-T018', async ({ registrationPage }) => {
            await registrationPage.enterConfirmPasswordField(registerData[1].confirmPassword);
        });
        test.skip('Enter a confirm password with white spaces R-T019', async ({ registrationPage }) => {
            await registrationPage.enterConfirmPasswordField(' '+ registerData[1].confirmPassword +' ');
        });
        test.skip('Test that the Privacy Policy are accepted R-T020', async ({ registrationPage }) => {
            await registrationPage.checkPrivacyPolicy();
        });
        test.skip('Verify that Privacy Policy is visible R-T021', async ({ registrationPage }) => {
            await registrationPage.privacyPolicyVisible();
        });
        test.skip('Register with valid account with Subscribe = No R-T022', async ({registrationPage }) => {
            await registrationPage.submitRegistrationForm();
            await registrationPage.checkNewsLetterNo();
            await registrationPage.clickContinueBtn();
            await registrationPage.validateSuccessufulRegistration();
        })
        test.skip('Register with existing account with Subscribe = No R-T023', async({registrationPage }) => {
            await registrationPage.registerWithExistingAccountSubscribe(registerData[0].firstName, registerData[0].lastName, registerData[0].email, registerData[0].telephone, registerData[0].password, registerData[0].confirmPassword);
            await registrationPage.checkNewsLetterNo();
            await registrationPage.clickContinueBtn();
            await registrationPage.registerWithExistingAccount();
        })
        test.skip('Register with valid account with Subscribe = Yes R-T024', async ({page, registrationPage}) => {
            await registrationPage.submitRegistrationForm();
            await registrationPage.checknewsLetterYes();
            await registrationPage.clickContinueBtn();
            await registrationPage.validateSuccessufulRegistration();
            await page.close();
        })
        test.skip('Register with existing account with Subscribe = Yes R-T025', async ({page, registrationPage}) => {
            await registrationPage.registerWithExistingAccountSubscribe(registerData[0].firstName, registerData[0].lastName, registerData[0].email, registerData[0].telephone, registerData[0].password, registerData[0].confirmPassword);
            await registrationPage.checknewsLetterYes();
            await registrationPage.clickContinueBtn();
            await registrationPage.registerWithExistingAccount();
            await page.close();
        })
        test.skip('Register with all empty fields with Subscribe = No R-T026', async ({page, registrationPage}) => {
            await registrationPage.clickContinueBtn();
            await registrationPage.registerWithEmptyFields();
            await page.close();
        })
        test.skip('Register with all empty fields with Subscribe = Yes R-T027', async ({page, registrationPage}) => {
            await registrationPage.checknewsLetterYes();
            await registrationPage.clickContinueBtn();
            await registrationPage.registerWithEmptyFields();
            await page.close();
        })
        test.skip('Register with existing user, Newsletter = Yes and Privacy Policy = Unchecked R-T028', async({page, registrationPage}) => {
            await registrationPage.checknewsLetterYes();
            await registrationPage.submitRegistrationFormUncheckPrivacyPlicy(registerData[0].firstName, registerData[0].lastName, registerData[0].email, registerData[0].telephone, registerData[0].password, registerData[0].confirmPassword);
            await registrationPage.clickContinueBtn();
            await registrationPage.privacyPolicyUnchecked();
            await page.close();
        })
        test.skip('Register with empty fields, Newsletter = Yes and Privacy Policy = Unchecked R-T029', async({page, registrationPage}) => {
            await registrationPage.checknewsLetterYes();
            await registrationPage.agreeToPrivacyPlicy();
            await registrationPage.clickContinueBtn();
            await registrationPage.submitEmptyFormPrivacyPolicyChecked();
            await page.close();
        })
})