import test, { expect } from "../fixtures/basePages";
test.describe('Account test suite', () => {
    test.beforeEach(async({page, accountPage}) => {
        await accountPage.goToAccountPage();
        // await page.pause();
    })
    test.afterEach(async({accountPage}) => {
        await accountPage.close();
    })
    test('Verify that uses is on the account page A-T001',async({accountPage}) => {
        await accountPage.verifyUserIsOnAccountPage();
    })
    test('Verify that users clickes on the Dashboard account page opens A-T002', async({accountPage}) => {
        await accountPage.hoverOverAccounttab();
        await accountPage.clickOnTheDashboard();
        await accountPage.verifyUserIsOnAccountPage();
    })
    test('Verify the nav-bar on the right side of the page A-T003',async({accountPage}) => {
        await accountPage.verifyRightNavBar();
    })
    test('Verify the number of menus on the right side A-T004',async({accountPage}) => {
        await accountPage.numberOfMenus();
    })
    test('Verify that My account tab is visible A-T005', async ({ accountPage }) => {
        await accountPage.myAccountTabVisible();
    });
    test('Verify the url of the Account Tab A-T006', async ({ accountPage }) => {
        await accountPage.verifyMyAccountTabUrl();
    });
    test('Verify that Edit Account Tab is visible A-T007', async ({ accountPage }) => {
        await accountPage.editAccountTabVisible();
    });
    test('Verify the url of the Edit Account Tab A-008', async ({ accountPage }) => {
        await accountPage.verifyEditAccountTabUrl();
    });
    test('Verify that Password Account tab is visible A-009', async ({ accountPage }) => {
        await accountPage.passwordAccountTabVisible();
    });
    test('Verify the Password Account tab url A-010', async ({ accountPage }) => {
        await accountPage.verifyPasswordAccountTabUrl();
    });
    test('Verify that Addressbook Account tab is visible A-011', async ({ accountPage }) => {
        await accountPage.verifyAdressBookAccountTabIsVisible();
    });
    test('Verify the Address Book Account tab url A-012', async ({ accountPage }) => {
        await accountPage.verifyAddressBookAccountTabUrl();
    });
    test('Verify that Wish List Account tab is visible A-013', async ({ accountPage }) => {
        await accountPage.wishListTabIsVisible();
    });
    test('Verify the Wish List Account tab url A-014', async ({ accountPage }) => {
        await accountPage.verifyWishListAccountTabUrl();
    });
    test('Verify that Order History tab is visible A-015', async ({ accountPage }) => {
        await accountPage.orderHistoryTabIsVisible();
    });
    test('Verify the Order List Account tab url A-016', async ({ accountPage }) => {
        await accountPage.verifyOrderHistoryAccountTabUrl();
    });
    test('Verify that Notificaion tab is visible A-017', async ({ accountPage }) => {
        await accountPage.notificationTabIsVisible();
    });
    test('Verify the Notification Account tab url A-018', async ({ accountPage }) => {
        await accountPage.verifyNotificationAccountTabUrl();
    });
    test('Verify that Downloads tab is visible A-019', async ({ accountPage }) => {
        await accountPage.downloadsTabIsVisible();
    });
    test('Verify the Downloads Account tab url A-020', async ({ accountPage }) => {
        await accountPage.verifyDownloadsAccountTabUrl();
    });
    test('Verify that Recurring payments tab is visible A-021', async ({ accountPage }) => {
        await accountPage.reaccuringPaymentsTabIsVisible();
    });
    test('Verify the Recurring payments tab url A-022', async ({ accountPage }) => {
        await accountPage.verifyreaccuringPaymentsAccountTabUrl();
    });
    test('Verify that Reward Points tab is visible A-023', async ({ accountPage }) => {
        await accountPage.rewardPointsTabIsVisible();
    });
    test('Verify the Reward Points tab url A-024', async ({ accountPage }) => {
        await accountPage.verifyRewardPointsAccountTabUrl();
    });
    test('Verify that Return tab is visible A-025', async ({ accountPage }) => {
        await accountPage.returnsTabIsVisible();
    });
    test('Verify the Return tab url A-026', async ({ accountPage }) => {
        await accountPage.verifyReturnsAccountTabUrl();
    });
    test('Verify that Transaction tab is visible A-027', async ({ accountPage }) => {
        await accountPage.transactionsTabIsVisible();
    });
    test('Verify the Transaction tab url A-028', async ({ accountPage }) => {
        await accountPage.verifyTransactionsAccountTabUrl();
    });
    test('Verify that Newsletter tab is visible A-029', async ({ accountPage }) => {
        await accountPage.verifyReturnsAccountTabUrl();
    });
    test('Verify the Newletter tab url A-030', async ({ accountPage }) => {
        await accountPage.verifyNewsletterAccountTabUrl();
    });
    test('Verify that Logout tab is visible A-031', async ({ accountPage }) => {
        await accountPage.logoutTabIsVisible();
    });
    test('Verify the Logout tab url A-032', async ({ accountPage }) => {
        await accountPage.verifyLogoutAccountTabUrl();
    });
});

