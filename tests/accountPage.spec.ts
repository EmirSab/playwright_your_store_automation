import test, { expect } from "../fixtures/basePages";
test.describe('Account test suite', () => {
    test.beforeEach(async({accountPage}) => {
        await accountPage.goToAccountPage();
    })
    test.afterEach(async({accountPage}) => {
        await accountPage.close();
    })
    test('Verify the number of icons on the page A-033', async ({ accountPage }) => {
        await accountPage.numberOfIcons();
    });
    test('Verify Edit Your Account Information is visible A-034', async ({ accountPage }) => {
        await accountPage.verifyEditYourAccountInformationIsVisible();
    });
    test('Verify Change your password is visible A-035', async ({ accountPage }) => {
        await accountPage.verifyChangeYourPasswordIsVisible();
    });
    test('Verify Change your password Url A-036', async ({ accountPage }) => {
        await accountPage.verifyChangeYourPasswordUrl();
    });
    test('Verify Modify your address book entries is visible A-037', async ({ accountPage }) => {
        await accountPage.modifyYourAddressBookEntriesIsVisible();
    });
    test('Verify Adress book Url A-038', async ({ accountPage }) => {
        await accountPage.modifyYourAddressBookEntriesUrl();
    });
    test('Verify wish list is visible A-039', async ({ accountPage }) => {
        await accountPage.modifyWishListIsVisible();
    });
    test('Verify Wish List Url A-040', async ({ accountPage }) => {
        await accountPage.modifyWishListsUrl();
    });
    test('Verify Subscribe / unsubscribe to newsletter is visible A-041', async ({ accountPage }) => {
        await accountPage.subscribeUnsubscribeToNewsletterIsVisible();
    });
    test('Verify Subscribe / unsubscribe to newsletter Url A-042', async ({ accountPage }) => {
        await accountPage.subscribeUnsubscribeToNewsletterUrl();
    });
    test('Verify View your order history is visible A-042', async ({ accountPage }) => {
        await accountPage.viewYourOrderHistoryIsVisible();
    });
    test('Verify View your order history Url A-043', async ({ accountPage }) => {
        await accountPage.viewYourOrderHistoryUrl();
    });
    test('Verify Downloads is visible A-044', async ({ accountPage }) => {
        await accountPage.downloadsIsVisible();
    });
    test('Verify Downloads Url A-045', async ({ accountPage }) => {
        await accountPage.downloadsUrl();
    });
    test('Verify Your Reward Points is visible A-046', async ({ accountPage }) => {
        await accountPage.yourRewardPointsIsVisible();
    });
    test('Verify Your Reward Points validate Url A-047', async ({ accountPage }) => {
        await accountPage.yourRewardPointsValidateUrl();
    });
    test('Verify View your return requests is visible A-048', async ({ accountPage }) => {
        await accountPage.viewYourReturnRequestsIsVisible();
    });
    test('Verify View your return requests validate Url A-049', async ({ accountPage }) => {
        await accountPage.viewYourReturnRequestsValidateUrl();
    });
    test('Verify Your Transactions is visible A-050', async ({ accountPage }) => {
        await accountPage.transactionsIsVisible();
    });
    test('Verify Your Transactions validate Url A-051', async ({ accountPage }) => {
        await accountPage.transactionsValidateUrl();
    });
    test('Verify Recurring payments is visible A-052', async ({ accountPage }) => {
        await accountPage.recurringPaymentsIsVisible();
    });
    test('Verify Recurring payments validate Url A-053', async ({ accountPage }) => {
        await accountPage.recurringPaymentsValidateUrl();
    });
    test('Verify Register for an affiliate account is visible A-054', async ({ accountPage }) => {
        await accountPage.registerForAnAffiliateAccountIsVisible();
    });
    test('Verify Register for an affiliate account validate Url A-055', async ({ accountPage }) => {
        await accountPage.registerForAnAffiliateAccountValidateUrl();
    });
});

