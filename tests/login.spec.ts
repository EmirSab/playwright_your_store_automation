import test, { expect } from "../fixtures/basePages";
import LoginPage from "../pom/LoginPage";
import ENV from '../utils/env';

const loginData = JSON.parse(JSON.stringify(require('../data/registrationData.json')));

test.describe('Login UI tests',() => {
    test.use({storageState: 'NoAuth.json'})
    test.beforeEach(async ({ page, navbarPage }) => {
        await page.goto(ENV.BASE_URL!);
        await navbarPage.cliclOnLogin();
    })

    test.afterEach(async({loginPage}) => {
        await loginPage.close();
    })
    test('Verify that login screen is displayed LI-T001',async ({loginPage}) => {
        await loginPage.loginScreenDisplayed();
    })
    test('Verify that login screen contains fields for entering username and password LI-T002',async ({loginPage}) => {
        await loginPage.loginScreenContainsEmailAndPasswordFileds();
    })
    test('Login with valid user account LI-T003', async({loginPage}) => {
        await loginPage.loginWithValidCredentials(loginData[0].email, loginData[0].password);
    })
    test('Login with empty email and password LI-T004', async({loginPage}) => {
        await loginPage.loginWithEmptyFields(loginData[1].email, loginData[1].password);
    })
    test('Login with empty password field LI-T005',async({loginPage}) => {
        await loginPage.loginWithEmptyPassword(loginData[0].email, loginData[1].password);
    })
    test('Login with empty email LI-T006', async({loginPage}) => {
        await loginPage.loginWithEmptyEmail(loginData[1].email,loginData[0].password);
    })
    test('Verify that Forgotten Password link is visible LI-T007', async({loginPage}) => {
        await loginPage.forgottenPasswordDisplayed();
    })
    test('Click on Forgotten Password link and verify the url LI-T008', async ({loginPage}) => {
        await loginPage.clickOnForgottenPassword();
        await loginPage.verifyForgottenPasswordUrl();
    });
    test('Verify that email field for forgotten password is displayed LI-T009', async ({loginPage}) => {
        await loginPage.forgottenPasswordEmailFieldDisplayed();
    });
    test('Vefiry if user enters empty field LI-T010', async ({loginPage}) => {
        await loginPage.clickOnForgottenPassword();
        await loginPage.clickOnContinueBtn()
        await loginPage.verifyEmailWasNotFound()
    });
    test('Vefify if the user enters incorrect email LI-T011', async ({loginPage}) => {
        await loginPage.clickOnForgottenPassword();
        await loginPage.enterForgottenPasswordEmail(loginData[2].email);
        await loginPage.clickOnContinueBtn();
        await loginPage.verifyEmailWasNotFound();
    });
    test('Forgotten Password test LI-T012', async({loginPage}) => {
        await loginPage.clickOnForgottenPassword();
        await loginPage.enterForgottenPasswordEmail(loginData[0].email);
        await loginPage.clickOnContinueBtn();
        await loginPage.verifyForgottenEmailSent();
    })
    test('Verify that email field is visible and editable LI-T013', async ({ loginPage }) => {
        await loginPage.emailFiledIsVisible();
        await loginPage.emailFieldEditable();
    });
    test('Verify that password field is visible and editable LI-T014', async ({ loginPage }) => {
        await loginPage.passwordFieldIsVisible();
        await loginPage.passwordFiledIsEditable();
    });
    test('Vefiry that password filed hides the characters LI-T015 ', async ({ loginPage }) => {
        await loginPage.passwordCharsHidden();
    });

    test('Verify that the login screen contains a "Register Account" link/button LI-T016', async({loginPage}) => {
        await loginPage.registerAccountVisible();
    })
    test('Verify that the "Register Account" link/button redirects the user to a registration screen LI-T017.', async ({ loginPage }) => {
        await loginPage.clickOnRegisterAccount();
    });
    test('Verify that the user is redirected to the home screen after a successful login LI-T018.', async ({ loginPage }) => {
        await loginPage.loginWithValidCredentials(loginData[0].email, loginData[0].password);
        await loginPage.userOnHomePage();
    });
    test('Verify that the user is not able to access the Order History without logging in first LI-T019.', async ({ loginPage }) => {
        await loginPage.goToOrderHistoryNotLoggedIn();
    });
    test('Verify that the user is logged out when they click the "Logout" button LI-T020.', async ({ loginPage }) => {
        await loginPage.clickOnMyAccount();
        await loginPage.loginWithValidCredentials(loginData[0].email, loginData[0].password);
        await loginPage.clickLogout();
    });
    test('Verify that the user is redirected to the logout screen after logging out LI-T021.', async ({ loginPage }) => {
        await loginPage.clickOnMyAccount();
        await loginPage.loginWithValidCredentials(loginData[0].email, loginData[0].password);
        await loginPage.clickLogout();
        await loginPage.userRedirectedToLogoutScreen();
    });
    test('Verify that user can navigate to login after logging out LI-T022.', async ({ loginPage }) => {
        await loginPage.clickOnMyAccount();
        await loginPage.loginWithValidCredentials(loginData[0].email, loginData[0].password);
        await loginPage.clickLogout();
        await loginPage.clickOnMyAccount();
        await loginPage.userIsOnLogginPage();
    });
    test('Verify that the user is able to log back in with their previous credentials after logging out LI-T023.', async ({ loginPage }) => {
        await loginPage.clickOnMyAccount();
        await loginPage.loginWithValidCredentials(loginData[0].email, loginData[0].password);
        await loginPage.clickLogout();
        await loginPage.clickOnMyAccount();
        await loginPage.clickOnLoginBtnAfterLogout();
        await loginPage.loginWithValidCredentials(loginData[0].email, loginData[0].password);
        await loginPage.clickLogout();
        await loginPage.clickOnMyAccount();
        await loginPage.userIsOnLogginPage();
    });
    test('Verify that email field is correct format LI-T024.', async ({ loginPage }) => {
        await loginPage.emailFieldFormat();
    });
    test('Verify that email address is in correct format LI-T025.', async ({ loginPage }) => {
        await loginPage.emailAddressCorrectFormat(loginData[0].email);
    });
    test('Verify that email address is not in correct format LI-T0026', async ({ loginPage }) => {
        await loginPage.emailAddressNotCorrectFormat(loginData[2].email);
    });
    test('Enter an email with white spaces LI-T027', async ({ loginPage }) => {
        await loginPage.enterEmail(loginData[3].email);
        await loginPage.emptyErrorMsg();
    });
})