import { Locator, Page, expect } from "@playwright/test";

export default class AccountPage {
    page:Page
    readonly accountPage:string;
    readonly getMyAccountTab:Locator;
    readonly dashboard: Locator;
    readonly rightSideNav: Locator;
    readonly menu:string;
    readonly myAccountTab: Locator;
    readonly editAccountTab: Locator;
    readonly passwordTab: Locator;
    readonly addressBookTab: Locator;
    readonly wishListTab: Locator;
    readonly orderHistoryTab: Locator;
    readonly notificationTab: Locator;
    readonly downloadsTab: Locator;
    readonly reaccuringPaymentsTab: Locator;
    readonly rewardPointsTab: Locator;
    readonly returnsTab: Locator;
    readonly transactionsTab: Locator;
    readonly newsletterTab: Locator;
    readonly logoutTab: Locator;

    readonly icons: Locator;
    readonly editYourAccountInformation: Locator;
    readonly editInformation: Locator;
    readonly changeYourPassword: Locator;
    readonly modifyYourAddressBookEntries:Locator;
    readonly wishList: Locator;
    readonly subscribeUnsubscribeToNewsletter: Locator;
    readonly viewYourOrderHistory: Locator;
    readonly downloads: Locator;
    readonly yourRewardPoints: Locator;
    readonly viewYourReturnRequests: Locator;
    readonly transactions: Locator;
    readonly recurringPayments: Locator;
    readonly registerForAnAffiliateAccount: Locator;

    constructor(page:Page) {
        this.page = page;
        this.accountPage = 'https://ecommerce-playground.lambdatest.io/index.php?route=account/account';
        this.getMyAccountTab = page.getByRole('button', { name: ' My account' });
        this.dashboard = page.getByRole('link', { name: 'Dashboard' });
        this.rightSideNav = page.locator('div.list-group.mb-3');
        this.menu = 'Logout';

        this.myAccountTab = page.getByRole('link', { name: ' My Account' });
        this.editAccountTab = page.getByRole('link', { name: ' Edit Account' });
        this.passwordTab = page.getByRole('link', { name: ' Password' });
        this.addressBookTab = page.getByRole('link', { name: ' Address Book' });
        this.wishListTab = page.getByRole('link', { name: ' Wish List' });
        this.orderHistoryTab = page.getByRole('link', { name: ' Order History' });
        this.notificationTab = page.getByRole('link', { name: ' Notification' });
        this.downloadsTab = page.getByRole('link', { name: ' Downloads' }).last();
        this.reaccuringPaymentsTab = page.getByRole('link', { name: ' Recurring payments' });
        this.rewardPointsTab = page.getByRole('link', { name: ' Reward Points' });
        this.returnsTab = page.getByRole('link', { name: ' Returns' });
        this.transactionsTab = page.getByRole('link', { name: ' Transactions' });
        this.newsletterTab = page.getByRole('link', { name: ' Newsletter' });
        this.logoutTab = page.getByRole('link', { name: ' Logout' });

        this.icons = page.locator('div.card-body.text-center');
        this.editYourAccountInformation = page.getByRole('link', { name: ' Edit your account information' });
        this.editInformation = page.getByText('Edit Information');
        this.changeYourPassword = page.getByRole('link', { name: ' Change your password' });
        this.modifyYourAddressBookEntries = page.getByRole('link', { name: ' Modify your address book entries' });
        this.wishList = page.getByRole('link', { name: ' Modify your wish list' });
        this.subscribeUnsubscribeToNewsletter = page.getByRole('link', { name: ' Subscribe / unsubscribe to newsletter' });
        this.viewYourOrderHistory = page.getByRole('link', { name: ' View your order history' });
        this.downloads = page.locator('#content').getByRole('link', { name: ' Downloads' });
        this.yourRewardPoints = page.getByRole('link', { name: ' Your Reward Points' });
        this.viewYourReturnRequests = page.getByRole('link', { name: ' View your return requests' });
        this.transactions = page.getByRole('link', { name: ' Your Transactions' });
        this.recurringPayments = page.getByRole('link', { name: ' Recurring payments' });
        this.registerForAnAffiliateAccount = page.getByRole('link', { name: ' Register for an affiliate account' });
    }

    //#region Register for an affiliate account
    async registerForAnAffiliateAccountIsVisible() {
        await expect(this.registerForAnAffiliateAccount).toBeVisible();
    }
    async registerForAnAffiliateAccountValidateUrl() {
        await this.registerForAnAffiliateAccount.click();
        await expect(this.page.url()).toContain('affiliate');
    }
    //#endregion

    //#region Recurring payments
    async recurringPaymentsIsVisible() {
        await expect(this.recurringPayments).toBeVisible();
    }
    async recurringPaymentsValidateUrl() {
        await this.recurringPayments.click();
        await expect(this.page.url()).toContain('recurring');
    }
    //#endregion

    //#region Your Transactions
    async transactionsIsVisible() {
        await expect(this.transactions).toBeVisible();
    }
    async transactionsValidateUrl() {
        await this.transactions.click();
        await expect(this.page.url()).toContain('transaction');
    }
    //#endregion

    //#region View your return requests
    async viewYourReturnRequestsIsVisible() {
        await expect(this.viewYourReturnRequests).toBeVisible();
    }

    async viewYourReturnRequestsValidateUrl() {
        await this.viewYourReturnRequests.click();
        await expect(this.page.url()).toContain('return');
    }
    //#endregion

    //#region Your Reward Points
    async yourRewardPointsIsVisible() {
        await expect(this.yourRewardPoints).toBeVisible();
    }
    async yourRewardPointsValidateUrl() {
        await this.yourRewardPoints.click();
        await expect(this.page.url()).toContain('reward');
    }
    //#endregion

    //#region Downloads
    async downloadsIsVisible() {
        await expect(this.downloads).toBeVisible();
    }
    async downloadsUrl() {
        await this.downloads.click();
        await expect(this.page.url()).toContain('download');
    }
    //#endregion

    //#region View your order history
    async viewYourOrderHistoryIsVisible() {
        await expect(this.viewYourOrderHistory).toBeVisible();
    }
    async viewYourOrderHistoryUrl() {
        await this.viewYourOrderHistory.click();
        await expect(this.page.url()).toContain('order');
    }
    //#endregion

    //#region Subscribe / unsubscribe to newsletter
    async subscribeUnsubscribeToNewsletterIsVisible() {
        await expect(this.subscribeUnsubscribeToNewsletter).toBeVisible();
    }

    async subscribeUnsubscribeToNewsletterUrl() {
        await this.subscribeUnsubscribeToNewsletter.click();
        await expect(this.page.url()).toContain('newsletter');
    }
    //#endregion

    //#region Wish List
    async modifyWishListIsVisible() {
        await expect(this.wishList).toBeVisible();
    }
    async modifyWishListsUrl() {
        await this.wishList.click();
        await expect(this.page.url()).toContain('wishlist');
    }
    //#endregion

    //#region Modify your address book entries
    async modifyYourAddressBookEntriesIsVisible() {
        await expect(this.modifyYourAddressBookEntries).toBeVisible();
    }
    async modifyYourAddressBookEntriesUrl() {
        await this.modifyYourAddressBookEntries.click();
        await expect(this.page.url()).toContain('address');
    }
    //#endregion
    //#region Change your password
    async verifyChangeYourPasswordIsVisible() {
        await expect(this.changeYourPassword).toBeVisible();
    }
    async verifyChangeYourPasswordUrl() {
        await this.changeYourPassword.click();
        await expect(this.page.url()).toContain('password');
    }
    //#endregion
    //#region Edit your account information
    async verifyEditYourAccountInformationIsVisible() {
        await this.page.waitForLoadState("domcontentloaded");
        await expect(this.editYourAccountInformation).toBeVisible();
    }
    //#endregion

    //#region Verify the three tabs on the page
    async numberOfIcons() {
        await this.page.waitForLoadState("domcontentloaded");
        const nextDiv = this.icons.locator('div.row');
        const nameOfIcons = nextDiv.locator('div');
        let isPresent = false;
         const number = await nameOfIcons.count();
        //  console.log(number);
        if(number > 10) {
            isPresent = true;
            await expect(isPresent).toEqual(true);
        } else {
            throw new Error("There was something wrong with the test!!");
        }
    }
    //#endregion

    //#region Logout
    async logoutTabIsVisible() {
        await expect(this.logoutTab).toBeVisible();
    }
    async verifyLogoutAccountTabUrl() {
        await this.logoutTab.click();
        await expect(this.page.url()).toContain('logout');
    }
    //#endregion

    //#region Newsletter
    async newsletterTabIsVisible () {
        await expect(this.newsletterTab).toBeVisible();
    }
    async verifyNewsletterAccountTabUrl() {
        await this.newsletterTab.click();
        await expect(this.page.url()).toContain('newsletter');
    }
    //#endregion


    //#region Transactions
    async transactionsTabIsVisible() {
        await expect(this.transactionsTab).toBeVisible();
    }
    async verifyTransactionsAccountTabUrl () {
        await this.transactionsTab.click();
        await expect(this.page.url()).toContain('transaction');
    }
    //#endregion

    //#region Returns
    async returnsTabIsVisible () {
        await expect(this.returnsTab).toBeVisible();
    }
    async verifyReturnsAccountTabUrl () {
        await this.returnsTab.click();
        await expect(this.page.url()).toContain('return');
    }
    //#endregion

    //#region Reward Points
    async rewardPointsTabIsVisible () {
        await expect(this.rewardPointsTab).toBeVisible();
    }
    async verifyRewardPointsAccountTabUrl () {
        await this.rewardPointsTab.click();
        await expect(this.page.url()).toContain('reward');
    }
    //#endregion

    //#region Recurring payments
    async reaccuringPaymentsTabIsVisible() {
        await expect(this.reaccuringPaymentsTab).toBeVisible();
    }
    async verifyreaccuringPaymentsAccountTabUrl() {
        await this.reaccuringPaymentsTab.click();
        await expect(this.page.url()).toContain('recurring');
    }
    //#endregion

    //#region Downloads tab
    async downloadsTabIsVisible () {
        await expect(this.downloadsTab).toBeVisible();
    }
    async verifyDownloadsAccountTabUrl () {
        await this.downloadsTab.click();
        await expect(this.page.url()).toContain('download');
    }
    //#endregion

    //#region Notification Tab
    async notificationTabIsVisible () {
        await expect(this.notificationTab).toBeVisible();
    }
    async verifyNotificationAccountTabUrl () {
        await this.notificationTab.click();
        await expect(this.page.url()).toContain('notification');
    }
    //#endregion

    //#region Order History Tab
    async orderHistoryTabIsVisible () {
        await expect(this.orderHistoryTab).toBeVisible();
    }
    async verifyOrderHistoryAccountTabUrl () {
        await this.orderHistoryTab.click();
        await expect(this.page.url()).toContain('order');
    }
    //#endregion

    //#region Wish List tab
    async wishListTabIsVisible() {
        await expect(this.wishListTab).toBeVisible();
    }
    async verifyWishListAccountTabUrl() {
        await this.wishListTab.click();
        await expect(this.page.url()).toContain('wish');
    }
    //#endregion

    //#region Adressbook Account Tab
    async verifyAdressBookAccountTabIsVisible() {
        await expect(this.addressBookTab).toBeVisible();
    }
    async verifyAddressBookAccountTabUrl() {
        await this.addressBookTab.click();
        await expect(this.page.url()).toContain('address');
    }
    //#endregion

    //#region Password Account Tab
    async passwordAccountTabVisible() {
        await expect(this.passwordTab).toBeVisible();
    }
    async verifyPasswordAccountTabUrl() {
        await this.passwordTab.click();
        await expect(this.page.url()).toContain('password');
    }
    //#endregion

    //#region Edit Account Tab
    async editAccountTabVisible() {
        await expect(this.editAccountTab).toBeVisible();
    }
    async verifyEditAccountTabUrl() {
        await this.editAccountTab.click();
        await expect(this.page.url()).toContain('edit');
    }
    //#endregion
    
    //#region My Account Tab
    async myAccountTabVisible() {
        await expect(this.myAccountTab).toBeVisible();
    }
    async verifyMyAccountTabUrl() {
        await this.myAccountTab.click();
        await expect(this.page.url()).toContain('account')
    }
    //#endregion
    //#region Verify numer of menus and menus
    async numberOfMenus() {
        const numberOfLinks = this.rightSideNav.locator('a');
        let isPresent = false;
        const number = await numberOfLinks.count();
        if(number > 10) {
            isPresent = true;
            await expect(isPresent).toEqual(true);
        } else {
            throw new Error("There was something wrong with the test!!");
        }
    }
    //#endregion

    //#region Right side nav-bar
    async verifyRightNavBar() {
        let isPresent = false;
        const menuName = this.menu;
        const navLinks = this.rightSideNav.locator('a').allTextContents();
        (await navLinks).filter(function(el){
            if(el === menuName) {
                isPresent = true;
                expect(isPresent).toEqual(true);
            }
            else {
                isPresent = false;
            }
        });
    }
    //#endregion

    async hoverOverAccounttab() {
        await this.getMyAccountTab.hover();
    }
    async clickOnTheDashboard() {
        await this.dashboard.waitFor({state: "visible"});
        await this.dashboard.click();
    }
    //#region Visit account page
    async goToAccountPage() {
        await this.page.goto(this.accountPage);
    }
    async verifyUserIsOnAccountPage() {
        await expect(this.page.url()).toContain('account/account')
    }
    //#endregion

    //#region Closing the page
    async close() {
        await this.page.close();
    }
    //#endregion
}