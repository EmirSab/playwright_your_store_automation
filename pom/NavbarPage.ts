import { expect, Locator, Page } from "@playwright/test";

export default class NavbarPage {
    private page: Page;
    readonly getMyAccountTab: Locator;
    readonly getRegisterTab: Locator;
    readonly getLoginTab: Locator;

    constructor(page: Page) {
        this.page = page;
        this.getMyAccountTab = page.getByRole('button', { name: ' My account' });
        this.getRegisterTab = page.locator('span', {hasText: 'Register'});
        this.getLoginTab = page.locator('span', {hasText: 'Login'});

    }
    private async clickOnMyAccount() {
        const myAccountTab = this.getMyAccountTab;
        await myAccountTab.waitFor();
        await myAccountTab.hover();
    }

    async clickOnRegister() {
        this.clickOnMyAccount();
        const registrationTab = this.getRegisterTab;
        await registrationTab.waitFor();
        await registrationTab.click();
        expect(this.page.url()).toContain('register');
    }

    async cliclOnLogin() {
        this.clickOnMyAccount();
        const loginTab = this.getLoginTab;
        await loginTab.waitFor();
        await loginTab.click();
        expect(this.page.url()).toContain('login');
    }
}