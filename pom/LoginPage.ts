import { expect, Locator, Page } from "@playwright/test";

export default class LoginPage {
    page: Page;

    readonly email: Locator;
    readonly password: Locator;
    readonly lgnBtn: Locator;
    readonly warrningMsg: Locator;
    readonly warrningMsgPass: Locator;
    readonly forgottenPassword: Locator;
    readonly forgottenPasswordEmail: Locator;
    readonly forgottenPasswordContinueBtn: Locator;
    readonly warrningEmailNotFound: Locator;
    readonly successForgottenEmailSent: Locator;
    readonly registerAccount: Locator;
    readonly registerAccountContinueBtn: Locator;
    readonly editAccInfo: Locator;
    readonly orderHistory: string;
    readonly getMyAccountTab: Locator;
    readonly logout: Locator;
    readonly logoutMsg: Locator;
    readonly loginBtn: Locator;
    readonly regex: RegExp;


    constructor(page: Page){
        this.page = page;

        this.email = page.locator('#input-email');
        this.password = page.locator('#input-password');
        this.lgnBtn = page.getByRole('button', { name: 'Login' });
        this.warrningMsg = this.page.getByText('Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.'); //all else
        this.warrningMsgPass = this.page.getByText('Warning: No match for E-Mail Address and/or Password.');
        this.forgottenPassword = this.page.locator('#content').getByRole('link', { name: 'Forgotten Password' });
        this.forgottenPasswordEmail = this.page.getByPlaceholder('E-Mail Address');
        this.forgottenPasswordContinueBtn = this.page.getByRole('button', { name: 'Continue' });
        this.warrningEmailNotFound = this.page.getByText('Warning: The E-Mail Address was not found in our records, please try again!');
        this.successForgottenEmailSent = this.page.getByText('An email with a confirmation link has been sent your email address.');
        this.registerAccount = this.page.getByText('Register Account');
        this.registerAccountContinueBtn = this.page.getByRole('link', { name: 'Continue' });
        this.editAccInfo = this.page.getByRole('link', { name: ' Edit your account information' });
        this.orderHistory = 'https://ecommerce-playground.lambdatest.io/index.php?route=account/order';

        this.getMyAccountTab = this.page.getByRole('button', { name: ' My account' });
        this.logout = this.page.locator('#widget-navbar-217834').getByRole('link', { name: 'Logout' });
        this.logoutMsg = this.page.getByText('You have been logged off your account. It is now safe to leave the computer.');
        this.loginBtn = this.page.locator('#widget-navbar-217834').getByRole('link', { name: 'Login' });
        this.regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    }

    async emailAddressNotCorrectFormat(email:string) {
        let notCorrect;
        if(!email.toLocaleLowerCase().match(this.regex)) {
            notCorrect = false;
            await expect(notCorrect).toBeFalsy();
        }
    }

    async emailAddressCorrectFormat(email: string) {
        let isFormat;
        if(email.toLocaleLowerCase().match(this.regex)) {
            isFormat = true;
            await expect(isFormat).toBeTruthy();
        }
    }

    async emailFieldFormat() {
        await expect(this.email).toHaveAttribute('name', 'email');
        await expect(this.email).toHaveAttribute('placeholder', 'E-Mail Address');
    }

    async clickOnLoginBtnAfterLogout() {
        await this.loginBtn.click();
    }

    async userIsOnLogginPage() {
        await this.page.url().includes('account/login');
    }

    async userRedirectedToLogoutScreen() {
        await this.page.url().includes('account/logout');
    }

    async logoutMsgVisible() {
        await expect(this.logoutMsg).toBeVisible();
    }

    async clickLogout() {
        await this.page.waitForURL('https://ecommerce-playground.lambdatest.io/index.php?route=account/account');
        await this.getMyAccountTab.hover();
        await this.logout.click({timeout:3000});
        await this.logoutMsgVisible();
    }

    async clickOnMyAccount() {
        const myAccountTab = this.getMyAccountTab;
        await myAccountTab.waitFor();
        await myAccountTab.hover();
    }

    async goToOrderHistoryNotLoggedIn() {
        await this.page.goto(this.orderHistory);
        await this.page.url().includes('account/login');
    }


    async userOnHomePage() {
        await this.page.url().includes('account/account');
    }

    async clickOnRegisterAccount() {
        await this.registerAccountContinueBtn.click();
        await expect(this.page).toHaveURL(/register/);
    }

    async registerAccountVisible() {
        await expect(this.registerAccount).toBeVisible();
        await expect(this.registerAccountContinueBtn).toBeVisible();
    }

    async passwordCharsHidden() {
        await expect(this.password).toHaveAttribute('type', 'password');
    }

    async passwordFieldIsVisible() {
        await expect(this.password).toBeVisible();
    }
    async passwordFiledIsEditable() {
        await expect(this.password).toBeEditable();
    }

    async emailFiledIsVisible() {
        await expect(this.email).toBeVisible();
    }

    async emailFieldEditable() {
        await expect(this.email).toBeEditable();
    }

    async verifyForgottenEmailSent() {
        await expect(this.successForgottenEmailSent).toBeVisible();
    }

    async enterForgottenPasswordEmail(enterEmail: string) {
        await this.page.waitForLoadState('networkidle');
        await this.forgottenPasswordEmail.waitFor();
        await this.forgottenPasswordEmail.fill(enterEmail);
    }

    async verifyEmailWasNotFound() {
        await expect(this.warrningEmailNotFound).toBeVisible();
    }

    async clickOnContinueBtn() {
        await this.forgottenPasswordContinueBtn.click();
    }

    async verifyForgottenPasswordUrl() {
        await expect(this.page).toHaveURL(/forgotten/);
    }

    async clickOnForgottenPassword() {
        await this.forgottenPassword.click();
    }
    async forgottenPasswordDisplayed() {
        await expect(this.forgottenPassword).toBeVisible();
    }
    
    async forgottenPasswordEmailFieldDisplayed() {
        await this.clickOnForgottenPassword();
        await expect(this.forgottenPasswordEmail).toBeVisible();
    }

    async loginScreenDisplayed() {
        await expect(this.page).toHaveURL(/login/);
    }

    async loginScreenContainsEmailAndPasswordFileds() {
        await expect(this.email).toBeVisible();
        await expect(this.password).toBeVisible();
    }
    async enterEmail(email: string) {
        await this.email.waitFor();
        await this.email.fill(email);
    }

    async enterPassword(password: string) {
        await this.password.waitFor();
        await this.password.fill(password);
    }

    async clickBtn() {
        await this.lgnBtn.click();
    }

    async loginWithValidCredentials(email: string, password: string) {
        await this.enterEmail(email);
        await this.enterPassword(password);
        await this.clickBtn();
        await this.validateUserIsLogged();
    }

    async validateUserIsLogged() {
        await expect(this.page).toHaveURL(/account/, {timeout: 3000});
    }

    //#region double expect
    //the reason for this is that devs made such a feature that sometimes it shows one message and other time different message
    //thus if someone runs this test I donw want it to crash to I am checking for both messages 
    async emptyErrorMsg() {
        const yourAccMsg = await this.warrningMsg.isVisible();
        if(yourAccMsg == true) {
            await expect(this.warrningMsg).toHaveCount(1);
        }
        else {
            await this.warrningMsgPass.isVisible();
        }
    }
    //#endregion

    async validateEmptyFields() {
        await this.warrningMsg.waitFor();
        await expect(this.warrningMsg).toHaveCount(1);
    }
    async loginWithEmptyFields(email: string, password: string) {
        await this.enterEmail(email);
        await this.enterPassword(password);
        await this.clickBtn();
        await this.emptyErrorMsg();
        await this.page.close();
    }


    async loginWithEmptyPassword(email: string, password: string) {
        await this.enterEmail(email);
        await this.enterPassword(password);
        await this.clickBtn();
        await this.emptyErrorMsg();
        await this.page.close();
    }
    

    async loginWithEmptyEmail(email: string, password: string){
        await this.enterEmail(email);
        await this.enterPassword(password);
        await this.clickBtn();
        await this.emptyErrorMsg();
        await this.page.close();
    }

    async close() {
        await this.page.close();
    }
}