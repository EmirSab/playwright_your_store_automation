import { Locator, Page } from "@playwright/test";
import { expect } from "../fixtures/basePages";
const changeData = JSON.parse(JSON.stringify(require('../data/changeData.json')));
const loginData = JSON.parse(JSON.stringify(require('../data/editData.json')));
export default class EditYourAccountInformationPage {
    page:Page;

    readonly editYourAccountInformationPage: string;
    readonly firstName: Locator;
    readonly lastName: Locator;
    readonly email: Locator;
    readonly telephone: Locator;
    readonly firstNameWarrning: Locator;
    readonly lastNameWarrning: Locator;

    readonly continueBtn: Locator;
    readonly successMsg: Locator;
    //readonly random: string;
    constructor(page:Page) {
        this.page = page;
        this.editYourAccountInformationPage = 'https://ecommerce-playground.lambdatest.io/index.php?route=account/edit';
        this.firstName = page.getByPlaceholder('First Name');
        this.lastName = page.getByPlaceholder('Last Name');
        this.email = page.getByPlaceholder('E-Mail');
        this.telephone = page.getByPlaceholder('Telephone');
        this.firstNameWarrning = page.getByText('First Name must be between 1 and 32 characters!');
        this.lastNameWarrning = page.getByText('Last Name must be between 1 and 32 characters!');

        this.continueBtn = page.getByRole('button', { name: 'Continue' });
        this.successMsg = page.getByText('Success: Your account has been successfully updated.');
    }

    readonly random: string = (Math.random() + 1).toString(36).substring(7);

    //#region 
    // await page.getByRole('textbox', { name: 'Password*' }).click();
    // await page.getByPlaceholder('Password Confirm').click();
    // await page.getByRole('button', { name: 'Continue' }).click();
    // await page.getByRole('link', { name: ' Back' }).click();

    //   await page.getByText('E-Mail Address does not appear to be valid!').click();
    //   await page.getByText('Telephone must be between 3 and 32 characters!').click();
    //#endregion

    //fields try to enter empty fields,submit

    //#region edit filed values
    async editFirstName() {
        //edit first name
        await this.firstName.fill(changeData[0].firstName);
        //save changes
        await this.continueBtn.click();
        //verify message
        await expect(this.successMsg).toBeVisible();
        //go to the edit page
        await this.goToEditYourAccountInformationPage();
        //change field to the old value
        await this.firstName.fill(loginData[0].firstName);
        //save
        await this.continueBtn.click();
    }
    async editLastName() {
        await this.lastName.fill(changeData[0].lastName);
        await this.continueBtn.click();
        await expect(this.successMsg).toBeVisible();
        await this.goToEditYourAccountInformationPage();
        await this.lastName.fill(loginData[0].lastName);
        await this.continueBtn.click();
    }
    async editTelephone() {
        await this.telephone.fill(changeData[0].telephone);
        await this.continueBtn.click();
        await expect(this.successMsg).toBeVisible();
        await this.goToEditYourAccountInformationPage();
        await this.telephone.fill(loginData[0].telephone);
        await this.continueBtn.click();
    }
    async editEmail() {
        await this.email.fill(changeData[0].email);
        await this.continueBtn.click();
        await expect(this.successMsg).toBeVisible();
        await this.goToEditYourAccountInformationPage();
        await this.email.fill(loginData[0].email);
        await this.continueBtn.click();
        await expect(this.successMsg).toBeVisible();
        await expect(this.email).toEqual(loginData[0].email);
    }
    async editFirstNameEmptyValue() {
        await this.firstName.fill('');
        await this.continueBtn.click({delay:300});
        await expect(this.firstNameWarrning).toBeVisible();
    }
    async editLastNameEmptyValue() {
        await this.lastName.fill('');
        await this.continueBtn.click();
        await expect(this.lastNameWarrning).toBeVisible();
    }
    async editEmailEmptyValue() {

    }
    async editTelephoneEmptyValue() {
        
    }
    //#endregion

    //#region are the values from json and in the fields equal
    async firstNameEqual(firstName) {
        const firstNameFieldValue = await this.firstName.inputValue();
        await expect(firstNameFieldValue).toEqual(firstName);
    }
    async lastNameEqual(lastName) {
        const lastNameFieldValue = await this.lastName.inputValue();
        await expect(lastNameFieldValue).toEqual(lastName);
    }
    async emailEqual(email) {
        const emailFieldValue = await this.email.inputValue();
        await expect(emailFieldValue).toEqual(email);
    }
    async telephoneEqual(telephone) {
        const telephoneFieldValue = await this.telephone.inputValue();
        await expect(telephoneFieldValue).toEqual(telephone);
    }
    //#endregion

    //#region fileds format
    async telephoneFieldFormat() {
        await expect(this.telephone).toHaveAttribute('name', 'telephone');
        await expect(this.telephone).toHaveAttribute('placeholder', 'Telephone');
        await expect(this.telephone).toHaveAttribute('type', 'tel');
    }
    async emailFieldFormat() {
        await expect(this.email).toHaveAttribute('name', 'email');
        await expect(this.email).toHaveAttribute('placeholder', 'E-Mail');
    }
    //#endregion

    //#region fields are editable
    async firstNameIsEditable() {
        await expect(this.firstName).toBeEditable();
    }
    async lastNameIsEditable() {
        await expect(this.lastName).toBeEditable();
    }
    async emailIsEditable() {
        await expect(this.email).toBeEditable();
    }
    async telephoneIsEditable() {
        await expect(this.telephone).toBeEditable();
    }
    //#endregion

    //#region Fields have value
    async firstNameHasValue() {
        await expect(this.firstName).not.toBeEmpty();
    }
    async lastNameHasValue(){
        await expect(this.lastName).not.toBeEmpty();
    }
    async emailHasValue() {
        await expect(this.email).not.toBeEmpty();
    }
    async telephoneHasValue() {
        await expect(this.telephone).not.toBeEmpty();
    }
    //#endregion

    //#region Fields are visible
    async firstNameIsVisible() {
        await expect(this.firstName).toBeVisible();
    }
    async lastNameIsVisible() {
        await expect(this.lastName).toBeVisible();
    }
    async emailIsVisible() {
        await expect(this.email).toBeVisible();
    }
    async telephoneIsVisible() {
        await expect(this.telephone).toBeVisible();
    }
    //#endregion


    async goToEditYourAccountInformationPage() {
        await this.page.goto(this.editYourAccountInformationPage);
    }
    async close() {
        await this.page.close();
    }
}