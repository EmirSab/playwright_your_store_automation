import { expect, Locator, Page } from "@playwright/test";
import {v4 as uuidv4} from 'uuid'
export default class RegistrationPage {
    page: Page;
    readonly firstName: Locator;
    readonly lastName: Locator;
    readonly email: Locator;
    readonly telephone: Locator;
    readonly password: Locator;
    readonly confirmPassword: Locator;
    readonly newsLetterYes: Locator;
    readonly newsLetterNo: Locator;
    readonly privacyPlicy: Locator;
    readonly continueBtn: Locator;
    readonly existingAccount: Locator;
    readonly warningPrivacyPolicy: Locator;
    readonly warningFirstName: Locator;
    readonly warningLastName: Locator;
    readonly warningTelephone: Locator;
    readonly warningPassword: Locator;
    readonly warrningConfirmPassword: Locator;

    readonly randFirstName: string;
    readonly randLastName: string;
    readonly randEmail: string;
    readonly randTelephone: string;
    readonly radnPassword: string;
    readonly regex: RegExp;
    readonly phoneRegex: RegExp;

    constructor(page: Page) {
        this.page = page;
        this.firstName = this.page.locator('#input-firstname');
        this.lastName = this.page.locator('#input-lastname');
        this.email = this.page.locator('#input-email');
        this.telephone = this.page.locator('#input-telephone');
        this.password = this.page.locator('#input-password');
        this.confirmPassword = this.page.locator('#input-confirm');
        this.newsLetterYes = this.page.getByText('Yes');
        this.newsLetterNo = this.page.getByRole('group', { name: 'Newsletter' }).getByText('No');
        this.privacyPlicy = this.page.getByText('I have read and agree to the Privacy Policy');
        this.continueBtn = this.page.locator('[value=Continue]');
        this.existingAccount = this.page.getByText(' Warning: E-Mail Address is already registered!');
        this.warningPrivacyPolicy = this.page.getByText('Warning: You must agree to the Privacy Policy!');
        this.warningFirstName = this.page.getByText('First Name must be between 1 and 32 characters!');
        this.warningLastName = this.page.getByText('Last Name must be between 1 and 32 characters!');
        this.warningTelephone = this.page.getByText('Telephone must be between 3 and 32 characters!');
        this.warningPassword = this.page.getByText('Password must be between 4 and 20 characters!');
        this.regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        this.phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        this.warrningConfirmPassword = this.page.getByText('Password confirmation does not match password!');

        this.randFirstName = uuidv4();
        this.randLastName = uuidv4();
        const emailId = uuidv4();
        this.randEmail = emailId + "@mail.com";
        this.randTelephone = Math.floor(Math.random() * 1000000000).toString();
        this.radnPassword = uuidv4();

    }

    //#region Terms and Conditions
        async checkPrivacyPolicy() {
            await this.privacyPlicy.check();
            await expect(this.privacyPlicy).toBeChecked();
        }
        async privacyPolicyVisible() {
            await expect(this.privacyPlicy).toBeVisible();
        }
    //#endregion
    //#region Confirm password field
    async enterConfirmPasswordField(confirmPassword:string) {
        await this.firstName.fill(this.randFirstName);
        await this.lastName.fill(this.randLastName);
        await this.email.fill(this.randEmail);
        await this.telephone.fill(this.randTelephone);
        await this.password.fill(this.radnPassword);
        await this.confirmPassword.fill(confirmPassword);
        await this.clickContinueBtn();
        await this.confirmPasswordErrorMessage();
    }
    async confirmPasswordErrorMessage() {
        await expect(this.warrningConfirmPassword).toBeVisible();
    }
    async confirmPasswordFieldCharsHidden() {
        await expect(this.confirmPassword).toHaveAttribute('type', 'password');
    }
    async confirmPasswordIsEditable() {
        await expect(this.confirmPassword).toBeEditable();
    }
    async confirmPasswordIsVisible() {
        await expect(this.confirmPassword).toBeVisible();
    }
    //#endregion

    //#region password field
    async enterPasswordField(password: string) {
        await this.firstName.fill(this.randFirstName);
        await this.lastName.fill(this.randLastName);
        await this.email.fill(this.randEmail);
        await this.telephone.fill(this.randTelephone);
        await this.password.fill(password);
        await this.confirmPassword.fill(this.radnPassword);
        await this.clickContinueBtn();
        await this.passwordErrorMessage();
    }
    async passwordErrorMessage() {
        await expect(this.warningPassword).toBeVisible();
    }
    //#endregion
    async enterTelephoneFieldWhiteSpaces(telephone: string) {
        await this.firstName.fill(this.randFirstName);
        await this.lastName.fill(this.randLastName);
        await this.email.fill(this.randEmail);
        await this.telephone.fill(telephone);
        await this.password.fill(this.radnPassword);
        await this.confirmPassword.fill(this.radnPassword);
        await this.clickContinueBtn();
    }
    async registrationPasswordFieldCharsHidden() {
        await expect(this.password).toHaveAttribute('type', 'password');
    }
    async passwordFieldIsVisible() {
        await expect(this.password).toBeVisible();
    }
    async passwordFieldIsEditable() {
        await expect(this.password).toBeEditable();
    }
    async enterTelephoneField(telephone: string) {
        await this.firstName.fill(this.randFirstName);
        await this.lastName.fill(this.randLastName);
        await this.email.fill(this.randEmail);
        await this.telephone.fill(telephone);
        await this.password.fill(this.radnPassword);
        await this.confirmPassword.fill(this.radnPassword);
        await this.clickContinueBtn();
        await this.enterTelephoneFieldEmptyMessage();
    }
    async enterTelephoneFieldEmptyMessage() {
        await expect(this.warningTelephone).toBeVisible();
    }
    async phoneIsVisible() {
        await expect(this.telephone).toBeVisible();
    }
    async phoneIsEditable() {
        await expect(this.telephone).toBeEditable();
    }
    async enterRegistrationEmail(email: string) {
        await this.email.fill(email);
        await this.clickContinueBtn();
        await this.emailAddressNotCorrectFormat(email);
        await expect(this.page.url()).toContain('account/register');
    }
    async lastNameIsVisible() {
        await expect(this.lastName).toBeVisible();
    }
    async lastNameIsEditable() {
        await expect(this.lastName).toBeEditable();
    }
    async firstNameIsVisible() {
        await expect(this.firstName).toBeVisible();
    }
    async firstNameIsEditable() {
        await expect(this.firstName).toBeEditable();
    }
    async emailFiledIsVisible() {
        await expect(this.email).toBeVisible();
    }
    async emailFieldEditable() {
        await expect(this.email).toBeEditable();
    }
    async emailAddressNotCorrectFormat(email:string) {
        let notCorrect;
        if(!email.toLocaleLowerCase().match(this.regex)) {
            notCorrect = false;
            await expect(notCorrect).toBeFalsy();
        }
    }
    async phoneAddressCorrectFormat(phone: string) {
        let isFormat;
        if(phone.toLocaleLowerCase().match(this.phoneRegex)) {
            isFormat = true;
            await expect(isFormat).toBeTruthy();
        }
    }
    async emailAddressCorrectFormat(email: string) {
        let isFormat;
        if(email.toLocaleLowerCase().match(this.regex)) {
            isFormat = true;
            await expect(isFormat).toBeTruthy();
        }
    }
    async telephoneFieldFormat() {
        await expect(this.telephone).toHaveAttribute('name', 'telephone');
        await expect(this.telephone).toHaveAttribute('placeholder', 'Telephone');
        await expect(this.telephone).toHaveAttribute('type', 'tel');
    }
    async emailFieldFormat() {
        await expect(this.email).toHaveAttribute('name', 'email');
        await expect(this.email).toHaveAttribute('placeholder', 'E-Mail');
    }
    async enterFirstName(firstName) {
        await this.firstName.waitFor();
        await this.firstName.fill(firstName);
    }
    async enterLastName(lastName) {
        await this.lastName.waitFor();
        await this.lastName.fill(lastName);
    }
    async enterEmail(email) {
        await this.email.waitFor();
        await this.email.fill(email);
    }
    async enterTelephone(telephone) {
        await this.telephone.waitFor();
        await this.telephone.fill(telephone);
    }
    async enterPassword(password) {
        await this.password.waitFor();
        const inputPassword = await this.password.fill(password);
        return inputPassword;
    }
    async enterConfirmPassword(confirmPassword) {
        await this.confirmPassword.waitFor();
        const inputconfirmPassword = await this.confirmPassword.fill(confirmPassword);
        return inputconfirmPassword;
    }
    async passwordEqualConfirmPassword(password,confirmPassword) {
        if(password !== confirmPassword) {
            expect(await this.page.getByText('Password confirmation does not match password!')).toHaveCount(1);
        }
    }
    async checknewsLetterYes() {
        await this.newsLetterYes.waitFor();
        await this.newsLetterYes.check();
    }
    async checkNewsLetterNo() {
        await this.newsLetterNo.waitFor();
        await this.newsLetterNo.click();
    }
    async agreeToPrivacyPlicy() {
        await this.privacyPlicy.waitFor();
        await this.privacyPlicy.click();
    }
    
    async clickContinueBtn() {
        await this.continueBtn.waitFor();
        await this.continueBtn.click();
    }
    async validateSuccessufulRegistration() {
        //https://ecommerce-playground.lambdatest.io/index.php?route=account/success
        await expect(this.page).toHaveURL(/success/, {timeout: 3000});
    }
    async registerWithExistingAccount() {
        //Warning: E-Mail Address is already registered!
        await expect(this.existingAccount).toHaveCount(1);
    }
    async privacyPolicyUnchecked() {
        await this.warningPrivacyPolicy.waitFor();
        await expect(this.warningPrivacyPolicy).toHaveCount(1);
    }
    async registerWithEmptyFields() {
        await this.privacyPolicyUnchecked();
        await this.warningFirstName.waitFor();
        await expect(this.warningFirstName).toHaveCount(1);
        await this.warningLastName.waitFor();
        await expect(this.warningLastName).toHaveCount(1);
        await this.warningPassword.waitFor();
        await expect(this.warningPassword).toHaveCount(1);
        await this.warningTelephone.waitFor();
        await expect(this.warningTelephone).toHaveCount(1);

    }
    async submitRegistrationForm() {

        await this.enterFirstName(this.randFirstName.substring(0,7));
        await this.enterLastName(this.randLastName.substring(0,7));
        await this.enterEmail(this.randEmail);
        await this.enterTelephone(this.randTelephone);
        await this.enterPassword(this.radnPassword);
        await this.enterConfirmPassword(this.radnPassword);   
        await this.agreeToPrivacyPlicy();   
    }
    async registerWithExistingAccountSubscribe(firstName, lastName, email, telephone, password, confirmPassword) {
        await this.enterFirstName(firstName);
        await this.enterLastName(lastName);
        await this.enterEmail(email);
        await this.enterTelephone(telephone);
        await this.enterPassword(password);
        await this.enterConfirmPassword(confirmPassword);
        await this.agreeToPrivacyPlicy(); 
    }
    async submitRegistrationFormUncheckPrivacyPlicy(firstName, lastName, email, telephone, password, confirmPassword) {
        await this.enterFirstName(firstName);
        await this.enterLastName(lastName);
        await this.enterEmail(email);
        await this.enterTelephone(telephone);
        await this.enterPassword(password);
        await this.enterConfirmPassword(confirmPassword);   
    }
    async submitEmptyFormPrivacyPolicyChecked() {
        await this.warningFirstName.waitFor();
        await expect(this.warningFirstName).toHaveCount(1);
        await this.warningLastName.waitFor();
        await expect(this.warningLastName).toHaveCount(1);
        await this.warningPassword.waitFor();
        await expect(this.warningPassword).toHaveCount(1);
        await this.warningTelephone.waitFor();
        await expect(this.warningTelephone).toHaveCount(1);
    }
}