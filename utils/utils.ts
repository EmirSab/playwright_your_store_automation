import { expect } from "@playwright/test";

export async function isFieldEqual(locatorFieldValue, field) {
    const fieldValue = await locatorFieldValue.inputValue();
    await expect(fieldValue).toEqual(field);
}