import { Browser, FullConfig, Page, chromium, expect } from "@playwright/test";
import dotenv from  'dotenv'
async function globalSetup(config: FullConfig) {
    if(process.env.test_env) {
        dotenv.config({
            path: `.env.${process.env.test_env}`,
            override: true
        })
    }
    //#region Login
    const browser: Browser = await chromium.launch({headless:true});
    const context = await browser.newContext();
    const page: Page = await context.newPage();
    await page.goto("https://ecommerce-playground.lambdatest.io/index.php?route=account/login");
    // await page.context().storageState({path: 'notLlooggedInState.json'});
    await page.locator('#input-email').fill("test.pw1@hotmail.com");
    await page.locator('#input-password').fill("testAdmin12");
    await page.getByRole('button', { name: 'Login' }).click();
    await expect(page.url()).toContain('https://ecommerce-playground.lambdatest.io');

    //save the state of the webpage
    await page.context().storageState({path:'./LoginAuth.json'});
    await browser.close();
    //#endregion
}
export default globalSetup;
